<!DOCTYPE html>
<html lang="en" ng-app="qiscusTask">
<head>
	<meta charset="UTF-8">
	<title>Qiscus Task</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="container">
		<header>
			<img src="img/qiscustask-head.png" alt="Qiscus Task">
		</header>
		<div id="sub-header">
			<img src="img/subheader.png" alt="Qiscus Internal Task Manager">
		</div>
		<div id="content">
			<ul ng-controller="mainCtrl" id="task-lists">
				<li class='fx-bounce-normal fx-easing-bounce fx-speed-500' ng-repeat="m in members">
			  		<h3>{{ m.name }}</h3>
			  		<small>{{ m.email }}</small>
			  		<hr>
			  		<ul class="task-details">
			  			<li ng-repeat="task in m.tasks" class='fx-zoom-up fx-easing-bounce fx-speed-500'>
			  				<a href="#">
			  					<strong>{{ task.date }}</strong>
			  					<p>{{ task.detail }}</p>
			  					<i class="fa fa-chevron-circle-right"></i>
			  				</a>
			  			</li>
			  		</ul>
			    	<hr>
			    	<a href="#" class="load-more">load more</a>
			  	</li>
			</ul>
		</div>
	</div>
	<script src="js/main.js"></script>
</body>
</html>